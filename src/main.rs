mod rom;
mod memory_map;
mod memory_mapper;
mod cpu;

#[macro_use(matches)]
extern crate matches;

fn main() {
    println!("Hello, world!");
}
